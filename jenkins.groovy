import groovy.json.*

node{
    stage("1"){
        withCredentials([usernamePassword(credentialsId: 'T0Z-http', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        
            username = USERNAME
			password = PASSWORD
			echo "${PASSWORD}"
            echo "${USERNAME}"
            
            //this.curl "-X GET -v --user triple0zero:4y3259c5 http://192.168.194.128:8080/crumbIssuer/api/json"
     //       echo "Чекаем crumb"
            //sh "curl -X GET -v --user triple0zero:4y3259c5 http://192.168.194.128:8080/crumbIssuer/api/json"
       //     sh "curl -s http://triple0zero:4y3259c5@192.168.194.128:8080/crumbIssuer/api/xml"
            
         //   sh "curl -s http://triple0zero:4y3259c5@192.168.194.128:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,':',//crumb)" > JENKINS_CRUMB
           // println("JENKINS_CRUMB = ${JENKINS_CRUMB}")

        }
        
            echo "${password}"
            echo "${username}"


//curl "-X GET -v --user triple0zero:4y3259c5 http://192.168.194.128:8080/crumbIssuer/api/json"
//curl "--help"

    echo "Версия питона"
    sh "python --version"
    
//    echo "Чекаем crumb"
//    sh "curl -X GET -v --user triple0zero:4y3259c5 http://192.168.194.128:8080/crumbIssuer/api/json"
    
    echo "Чекаем последний номер билда"
    
    def url = "http://192.168.194.128:8080/job/STUB"
    def mycred = "T0Z-http"
    def lastNumber = getLastBuildVersion(this, url, mycred)
    
    println("последний номер билда ${lastNumber}")
    
    echo "Запускаем билд"
    
    def param = "${url}/buildWithParameters?p1=HELLO%26p2=WORLD"
    
    //def rrr = jobBuild(this, param, mycred)
    
    echo "All Done!!!"
    
    echo "Looking for LOCATION"
    //println(rrr)
    
    //sh "curl -X POST -v --user triple0zero:4y3259c5 http://192.168.194.128:8080/job/STUB/buildWithParameters?p1=HELLO"
    
    
      
//    withCredentials([usernamePassword(credentialsId: 'T0Z-http', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        
  //          echo "Запустим джобу внутри блока whithCred"
    //        username = USERNAME
	//		password = PASSWORD
	//		echo "${PASSWORD}"
    //        echo "${USERNAME}"
      //      sh "curl -X POST -v -u ${username}:${password} http://192.168.194.128:8080/job/STUB/buildWithParameters?p1=HELLO"
     

        //}
       // echo "Запустим джобу снаружи блока whithCred - РАБОТАЕТ!!!"
       //     echo "${password}"
       //     echo "${username}"
            
       //     sh "curl -X POST -v -u ${username}:${password} http://192.168.194.128:8080/job/STUB/buildWithParameters?p1=HELLO"
       
       tempfile = "${WORKSPACE}/out.txt"
       
       def rrr
       withCredentials([usernamePassword(credentialsId: "${mycred}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
       
         rrr = jobBuildCurl(this, param, USERNAME, PASSWORD, tempfile)
       }
       
       echo "А нет ли тут локейшн?"
       println(rrr)
       

    }
}

def static jobBuildCurl(pipelinescript, jobUrlWithParams, u, p, temp) {
    
      pipelinescript.echo "${u}"
      pipelinescript.echo "${p}"
      //def r = script.sh "curl -X POST -v -u ${u}:${p} ${jobUrlWithParams} | tee ${temp}"
      def response = pipelinescript.sh(script: "curl -X POST -v -k -u ${u}:${p} ${jobUrlWithParams}", returnStdout: true)
    
    return response

}



static void jobBuild(script, jobUrlWithParams, creds) {

    script.httpRequest(
            httpMode: "POST",
            authentication: creds,
            ignoreSslErrors: true,
            url: "${jobUrlWithParams}"
    )


}

def static getLastBuildVersion(script, jobUrl, creds) {

    def response = script.httpRequest(
            url: "${jobUrl}/lastBuild/api/json?tree=number",
            timeout: 120,
            authentication: creds,
            validResponseCodes: '200',
            ignoreSslErrors: true
    ).content.trim()
    def resJson = script.readJSON text: response

    return resJson 
}